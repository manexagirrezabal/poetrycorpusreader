import sys
import os
from lxml import etree
import re

'''
HELP FROM:
https://docs.python.org/2/library/xml.etree.elementtree.html#parsing-xml-with-namespaces
https://docs.python.org/2/library/xml.etree.elementtree.html
THE PROGRAM readFileFrozenPrintSyllables TAKES AS ARGUMENT AN XML FILE (FROM THE 4B4V POETRY CORPUS) AND IT PRINTS TWO COLUMNS [SYLLABLE\tSTRESS] WHICH WILL SERVE AS TRAINING CORPUS FOR HUNPOS-TRAIN, THE HMM BASED TAGGER
THE PROGRAM readFileFrozenPrintGoldForZeuScansion TAKES AS ARGUMENT AN XML FILE (FROM THE 4B4V POETRY CORPUS) AND IT PRINTS TWO COLUMNS [STRESS PATTERN\tLINE]
WHICH WILL SERVE AS GOLD STANDARD FOR ZEUSCANSION
'''

def readFileFrozenPrintSyllables (filename, verbose=False):
    stresses=[]
    lexicalstresses=[]
    wordType=[]
    words=[]

    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    ns={'ns':"http://www.w3.org/1999/xhtml"}

    lines = tree.findall("./body/div[@id='main']/ns:div[@id='poem']/ns:div[@class='prosody-line ']/ns:div[@class='TEI-l']", ns)
    for k in lines:

        realscansions = k.attrib['real'].split("|") #anbiguotasuna dagoenean, errepikatu esaldia eta analisi bakoitza jarri. When there's ambiguity, repeat the sentence whenever it's ambiguous
        syllables=k.findall("ns:span[@class='prosody-syllable']", ns)
        for realscansion in realscansions:
            for indsyllable, syllable in enumerate(syllables):
                print re.sub(r'[^\w]','',syllable.text).lower().encode("utf8")+"\t"+realscansion[indsyllable] #Remove everything that is not alphanumeric (!.;...)
            print

    print "1\t+" #DIGIT NECESARY FOR HUNPOS-TRAIN
    print "1\t-" #DIGIT NECESARY FOR HUNPOS-TRAIN


def readFileFrozenPrintGoldForZeuScansion (filename, verbose=False):
    stresses=[]
    lexicalstresses=[]
    wordType=[]
    words=[]

    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    ns={'ns':"http://www.w3.org/1999/xhtml"}

    lines = tree.findall("./body/div[@id='main']/ns:div[@id='poem']/ns:div[@class='prosody-line ']/ns:div[@class='TEI-l']", ns)
    for k in lines:
        line = "".encode("utf8")
        realscansions = k.attrib['real']
        syllables=k.findall("ns:span[@class='prosody-syllable']", ns)
        for indsyllable, syllable in enumerate(syllables):
            line = line + syllable.text
        print realscansions.encode("utf8")+"\t".encode("utf8")+line.encode("utf8")


def readFileFrozenPrintforDLsyllBysyll (filename, verbose=False):
    stresses=[]
    lexicalstresses=[]
    wordType=[]
    words=[]

    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    ns={'ns':"http://www.w3.org/1999/xhtml"}

    poemstruct = tree.findall("./body/div[@id='main']/ns:div[@id='poem']/", ns)
    lines = tree.findall("./body/div[@id='main']/ns:div[@id='poem']/ns:div[@class='prosody-line ']/ns:div[@class='TEI-l']", ns)
    linesandbreaks = [re.sub('\{.*\}','',i.tag) for i in poemstruct if (i.tag=="{"+ns['ns']+"}"+"br" or i.attrib.get('class','')=='prosody-line ')]
#    print [i.attrib.get('class', '') for i in poemstruct if (i.tag=="{"+ns['ns']+"}"+"br" or i.attrib.get('class','')=='prosody-line ')]

    indline=0
    for k in lines:

        realscansions = k.attrib['real'].split("|") #anbiguotasuna dagoenean, errepikatu esaldia eta analisi bakoitza jarri. When there's ambiguity, repeat the sentence whenever it's ambiguous
        syllables=k.findall("ns:span[@class='prosody-syllable']", ns)
        

#        line = "".encode("utf8")
        for realscansion in realscansions:
            line = "".encode("utf8")
            for indsyllable, syllable in enumerate(syllables):
                line = line + re.sub(r'[^\w]','',syllable.text).lower()+"_"+realscansion[indsyllable]+" "
                #print re.sub(r'[^\w]','',syllable.text).lower().encode("utf8")+"\t"+realscansion[indsyllable] #Remove everything that is not alphanumeric (!.;...)
            #print
            print line.encode("utf8")

        if linesandbreaks[indline] == 'br':
            indline+=1
            print
#        print line.encode("utf8")
        indline+=1
        
    print
    print
    print



def readFileFrozenPrintforDL (filename, verbose=False):
    stresses=[]
    lexicalstresses=[]
    wordType=[]
    words=[]

    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    ns={'ns':"http://www.w3.org/1999/xhtml"}

    poemstruct = tree.findall("./body/div[@id='main']/ns:div[@id='poem']/", ns)
    lines = tree.findall("./body/div[@id='main']/ns:div[@id='poem']/ns:div[@class='prosody-line ']/ns:div[@class='TEI-l']", ns)
    linesandbreaks = [re.sub('\{.*\}','',i.tag) for i in poemstruct if (i.tag=="{"+ns['ns']+"}"+"br" or i.attrib.get('class','')=='prosody-line ')]
#    print [i.attrib.get('class', '') for i in poemstruct if (i.tag=="{"+ns['ns']+"}"+"br" or i.attrib.get('class','')=='prosody-line ')]

    indline=0
    for k in lines:

        realscansions = k.attrib['real'].split("|") #anbiguotasuna dagoenean, errepikatu esaldia eta analisi bakoitza jarri. When there's ambiguity, repeat the sentence whenever it's ambiguous
        syllables=k.findall("ns:span[@class='prosody-syllable']", ns)
        #line = "".encode("utf8")

        for realscansion in realscansions:
        #FOR THE SAKE OF SIMPLICITY, LET'S FIRST TAKE THE FIRST ANALYSIS #TO-DO
            line = "".encode("utf8")
            for indsyllable, syllable in enumerate(syllables):
                line = line + realscansion[indsyllable]+re.sub(r'[^\w ]+',' ',syllable.text).lower()

            if linesandbreaks[indline] == 'br':
                indline+=1
                print
#        print ' '.join([removestresses(i)+extractstresses(i) for i in line.split(" ") if i!=''])
        #FORMAT FOR NLPNET (https://github.com/erickrf/nlpnet)
            print ' '.join([removestresses(i)+"_"+extractstresses(i) for i in line.split(" ") if (i!='' and extractstresses(i)!='' and removestresses(i)!='')]) #the AND here is really important, as it avoids empty elements in our learning environment
        indline+=1
        
    print
    print
    print

def extractstresses(s):
    return ''.join([i for i in s if i in '-+'])

def removestresses(s):
    return ''.join([i for i in s if i not in '-+'])


readFileFrozenPrintforDL(sys.argv[1])
#readFileFrozenPrintforDLsyllBysyll(sys.argv[1])
#readFileFrozenPrintGoldForZeuScansion(sys.argv[1])
