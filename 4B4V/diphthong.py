
# -*- coding:  utf8
import sys
import re

f=open(sys.argv[1])
lines = [re.sub("[^a-z\ ]", "", line.decode("utf8").rstrip().lower()) for line in f]
f.close()

def removeChars (w):
  return re.sub("[^aeiou]", "", w)

nuclei=[]
for line in lines:
#  print re.split("[^aeiou]", line)
  nuclei=nuclei+re.split("[^aeiou]", line)
#  nuclei=nuclei+[removeChars(i) for i in line.split()]

sortedset=sorted([i for i in list(set(nuclei)) if (len(i)<3 and len(i)>1)])

print sortedset
print ','.join(sortedset)
