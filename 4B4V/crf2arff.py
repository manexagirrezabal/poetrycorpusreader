import sys
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--file', help='Corpus name')
parser.add_argument('--currword', help='Include current word as feature',  action='store_true')
parser.add_argument('--currpos', help='Include current POS-tag as feature',  action='store_true')
parser.add_argument('--currsyllable', help='Include current syllable as feature',  action='store_true')
parser.add_argument('--currlexicalst', help='Include current lexical stress as feature',  action='store_true')
parser.add_argument('--contword', help='Include context words as features',  action='store_true')
parser.add_argument('--contpos', help='Include context POS-tags as features',  action='store_true')
parser.add_argument('--contsyllable', help='Include context syllables as features',  action='store_true')
parser.add_argument('--contlexicalst', help='Include context lexical stresses as features',  action='store_true')
args = parser.parse_args()


f=open(args.file)
lines = [line.decode("utf8").rstrip().replace("\"", "\\\"").split("\t") for line in f if line.rstrip() != '']
f.close()

print  "@RELATION ZeuScansionMLDataset"

currentWord=args.currword
currentPos=args.currpos
currentSyllable=args.currsyllable
currentLexicalstress=args.currlexicalst

contextWord=args.contword
contextPos=args.contpos
contextSyllable=args.contsyllable
contextLexicalstress=args.contlexicalst

if currentWord:
  print  "@ATTRIBUTE word-ts STRING"
if contextWord:
  print  "@ATTRIBUTE word-ts+1 STRING"
  print  "@ATTRIBUTE word-ts+2 STRING"
  print  "@ATTRIBUTE word-ts+3 STRING"
  print  "@ATTRIBUTE word-ts+4 STRING"
  print  "@ATTRIBUTE word-ts+5 STRING"
  print  "@ATTRIBUTE word-ts-1 STRING"
  print  "@ATTRIBUTE word-ts-2 STRING"
  print  "@ATTRIBUTE word-ts-3 STRING"
  print  "@ATTRIBUTE word-ts-4 STRING"
  print  "@ATTRIBUTE word-ts-5 STRING"

if currentLexicalstress:
  print  "@ATTRIBUTE LS-ts STRING"
if contextLexicalstress:
  print  "@ATTRIBUTE LS-ts+1 STRING"
  print  "@ATTRIBUTE LS-ts+2 STRING"
  print  "@ATTRIBUTE LS-ts+3 STRING"
  print  "@ATTRIBUTE LS-ts+4 STRING"
  print  "@ATTRIBUTE LS-ts+5 STRING"
  print  "@ATTRIBUTE LS-ts-1 STRING"
  print  "@ATTRIBUTE LS-ts-2 STRING"
  print  "@ATTRIBUTE LS-ts-3 STRING"
  print  "@ATTRIBUTE LS-ts-4 STRING"
  print  "@ATTRIBUTE LS-ts-5 STRING"

if currentPos:
  print  "@ATTRIBUTE POSTAG-ts STRING"
if contextPos:
  print  "@ATTRIBUTE POSTAG-ts+1 STRING"
  print  "@ATTRIBUTE POSTAG-ts+2 STRING"
  print  "@ATTRIBUTE POSTAG-ts+3 STRING"
  print  "@ATTRIBUTE POSTAG-ts+4 STRING"
  print  "@ATTRIBUTE POSTAG-ts+5 STRING"
  print  "@ATTRIBUTE POSTAG-ts-1 STRING"
  print  "@ATTRIBUTE POSTAG-ts-2 STRING"
  print  "@ATTRIBUTE POSTAG-ts-3 STRING"
  print  "@ATTRIBUTE POSTAG-ts-4 STRING"
  print  "@ATTRIBUTE POSTAG-ts-5 STRING"

if currentSyllable:
  print  "@ATTRIBUTE syllable-ts STRING"
if contextSyllable:
  print  "@ATTRIBUTE syllable-ts+1 STRING"
  print  "@ATTRIBUTE syllable-ts+2 STRING"
  print  "@ATTRIBUTE syllable-ts+3 STRING"
  print  "@ATTRIBUTE syllable-ts+4 STRING"
  print  "@ATTRIBUTE syllable-ts+5 STRING"
  print  "@ATTRIBUTE syllable-ts+6 STRING"
  print  "@ATTRIBUTE syllable-ts+7 STRING"
  print  "@ATTRIBUTE syllable-ts+8 STRING"
  print  "@ATTRIBUTE syllable-ts+9 STRING"
  print  "@ATTRIBUTE syllable-ts+10 STRING"
  print  "@ATTRIBUTE syllable-ts-1 STRING"
  print  "@ATTRIBUTE syllable-ts-2 STRING"
  print  "@ATTRIBUTE syllable-ts-3 STRING"
  print  "@ATTRIBUTE syllable-ts-4 STRING"
  print  "@ATTRIBUTE syllable-ts-5 STRING"
  print  "@ATTRIBUTE syllable-ts-6 STRING"
  print  "@ATTRIBUTE syllable-ts-7 STRING"
  print  "@ATTRIBUTE syllable-ts-8 STRING"
  print  "@ATTRIBUTE syllable-ts-9 STRING"
  print  "@ATTRIBUTE syllable-ts-10 STRING"


print  "@ATTRIBUTE last4-chars STRING"
print  "@ATTRIBUTE last3-chars STRING"
print  "@ATTRIBUTE syllable-num-line REAL"
print  "@ATTRIBUTE syllable-number-within-word REAL"
print  "@ATTRIBUTE word-length REAL"
print  "@ATTRIBUTE number-of-syllables-line REAL"
print  "@ATTRIBUTE last2-chars STRING"
print  "@ATTRIBUTE last5-chars STRING"
print  "@ATTRIBUTE isheavy STRING"
print  "@ATTRIBUTE last-char STRING"



#13 features
#print  "@ATTRIBUTE syllable-ts STRING"
#print  "@ATTRIBUTE syllable-ts-2 STRING"
#print  "@ATTRIBUTE syllable-ts-1 STRING"

#print  "@ATTRIBUTE last4-chars STRING"
#print  "@ATTRIBUTE last3-chars STRING"
#print  "@ATTRIBUTE last5-chars STRING"
#print  "@ATTRIBUTE syllable-number-within-word REAL"
#print  "@ATTRIBUTE postag STRING"
#print  "@ATTRIBUTE LSaft STRING"
#print  "@ATTRIBUTE wordlength REAL"
#print  "@ATTRIBUTE LSbef STRING"
#print  "@ATTRIBUTE LS STRING"
#print  "@ATTRIBUTE last2-chars STRING"
#print  "@ATTRIBUTE syllable-num-line REAL"
#print  "@ATTRIBUTE last1-chars STRING"
#print  "@ATTRIBUTE number-of-syllables-line REAL"

print  "@ATTRIBUTE class        {+,=}"
print
print
print "@DATA"
for kube in lines:
##All features
    t=0
    t=t+1
    if currentWord:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+1]])+",",
    t=t+1 #11
    if contextWord:
      #WORD WINDOW
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+10]])+",",
    t=t+10 #11

    if currentLexicalstress:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+1]])+",",
    t=t+1 #11
    if contextLexicalstress:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+10]])+",",
    t=t+10 #11

    if currentPos:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+1]])+",",
    t=t+1 #11
    if contextPos:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+10]])+",",
    t=t+10 #11

    if currentSyllable:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+1]])+",",
    t=t+1 #11
    if contextSyllable:
      print ','.join(["\""+i.encode("utf8")+"\"" for i in kube[t:t+20]])+",",
    t=t+20 #11

    #LAST 10 attributes
    print "\""+kube[t].encode("utf8")+"\",",
    t=t+1 #34

    print "\""+kube[t].encode("utf8")+"\",",
    t=t+1 #35

    print str(kube[t][4:])+",",
    t=t+1

    print str(kube[t][5:])+",",
    t=t+1

    print str(kube[t][5:])+",",
    t=t+1

    print str(kube[t][5:])+",",
    t=t+1

    print "\""+kube[t].encode("utf8")+"\",",
    t=t+1 #36

    print "\""+kube[t].encode("utf8")+"\",",
    t=t+1 #37

    print "\""+kube[t].encode("utf8")+"\",",
    t=t+1 #37

    print "\""+kube[t].encode("utf8")+"\",",
    t=t+1


    print kube[0].encode("utf8")


