import sys
import os
from lxml import etree
import re
import itertools
import nltk
import HTMLParser
h = HTMLParser.HTMLParser()

import newCorpusReader

import lexicon
HOME = os.environ['HOME']
lex=lexicon.lexicon(HOME+"/Dropbox/bertsobot/pronun/hiztegiak/newdic-ipa.txt")

fn={}
#fn['CLASS']=0
fn['SYLLNUMINLINE']=3
fn['LS']=4
fn['POS']=5
fn['WLEN']=6
fn['LSBEF']=13
fn['LSAFT']=14
fn['SYLLNUMWORD']=7
fn['SYLLNUMLINE']=15
fn['LASTCHAR']=8
fn['LAST2CHAR']=9
fn['LAST3CHAR']=10
fn['LAST4CHAR']=11
fn['LAST5CHAR']=12


'''
HELP FROM:
https://docs.python.org/2/library/xml.etree.elementtree.html#parsing-xml-with-namespaces
https://docs.python.org/2/library/xml.etree.elementtree.html
THE PROGRAM printFeaturesFromFile TAKES AS ARGUMENT AN XML FILE (FROM THE 4B4V POETRY CORPUS) AND A FORMAT (hunpos, crf, DLs2s, DLw2w).
IT PRINTS THE FILE IN THE SPECIFIED FORMAT.
  -hunpos: The format in which the hunpos-implementation works. It's an HMM-based tagger
  -crf: The format for the crfsuite program. CRF-based implementation.
  -DLs2s: This prints the poem in a special format to use in the char-rnn package (Syllable by syllable). Example: "that_= is_+ the_= ques_+ tion_="
  -DLw2w: This prints the poem in a special format to use in the char-rnn package (Word by word). Example: "that_= is_+ the_= question_+="
'''

'''
This file gets an XML file from the 4B4V repository with analyzed English poems and it extracts the syllables, words and stresses in tuples/words/lines/linegroups
'''
def readFileGetSyllableStressWords (filename, verbose=False):
    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    if "{" in tree.tag:
        namespace= re.sub("\}.*", "", tree.tag).replace("{","")
    else:
        namespace=''
    ns = {'ns':namespace}

    nolines=0
    noambiguouslines=0
    noambiguouslineslevel2=0
    nononambiguouslines=0
    weirdlines=0

    lineElements=[]
    lineGroupElements=[]
    linegroups = tree.findall("./ns:text/ns:body/", ns)
    for linegroup in linegroups:
        for line in linegroup.findall("./ns:l", ns):
            nolines=nolines+1
            realscansions = line.attrib['real'].split("|")
            realscansions = [i.replace("(", "") for i in realscansions]
            realscansions = [i.replace(")", "") for i in realscansions]
            realscansions = [i.replace(" ", "") for i in realscansions]

            #REPLACE - with = to avoid ambiguity: Ah Sun-flower! weary of time,
            realscansions = [i.replace("-", "=") for i in realscansions]

            if len(realscansions) ==2:
                noambiguouslines=noambiguouslines+1
            elif len(realscansions) >2:
                noambiguouslineslevel2=noambiguouslineslevel2+1
                print realscansions
            elif len(realscansions) == 1:
                nononambiguouslines=nononambiguouslines+1
            else:
                weirdlines=weirdlines+1

            segV=[]
            for realscansion in realscansions:
                scannedline=""
                stressind=0
                segments = line.findall("./ns:seg", ns)
                for segno, seg in enumerate(segments): #EACH SEGMENT / EACH FOOT
                    for kno, k in enumerate(seg.itertext()): #EACH SEGMENT OR ELEMENT INSIDE SEGMENT
                        if hasChar(k):
                            #print "YES_"+k+"_", stressind, realscansion, len(realscansion)
                            scannedsegment,stressindloc=alignLineStress(realscansion[stressind:], k)
                            stressind=stressind+stressindloc
                            scannedline=scannedline+scannedsegment
                        #else:
                            #print "NO_"+k+"_", stressind, realscansion
                lineElements.append([extractInfo(i) for i in scannedline.split()])
        lineGroupElements.append(lineElements)
        lineElements=[]

#    print "Stats for poem",filename,":"
#    print "Number of lines:",nolines
#    print "Number of ambiguous lines:",noambiguouslines
#    print "Number of non-ambiguous lines:",nononambiguouslines
#    print "Number of weird lines:",weirdlines
    return nolines, noambiguouslines, noambiguouslineslevel2, nononambiguouslines, weirdlines, 



def extractInfo(str):
    stresses=extractstresses(str)
    word=removestresses(str)
    syllables=re.split("[=|+]", str)[:-1]
    return zip(stresses,syllables,[word]*len(syllables))

def alignLineStress (stresses, line):
  i=0
  pos=0
  lineind=0
  while i<len(line) and pos != -1:
    pos=line.find(" ",i)
    if pos != -1:
      line=line[:pos]+stresses[lineind]+line[pos:]
      lineind=lineind+1
      i=pos+2
    else:
      line=line+stresses[lineind]
      lineind=lineind+1

  return line, lineind

def extractstresses(s):
    return ''.join([i for i in s if i in '=+'])

def removestresses(s):
    return ''.join([i for i in s if i not in '=+'])

def hasChar (s):
    for c in s:
        if c.isalpha():
            return True
    return False

def checkAmbiguity (filedir):
    import glob
    stats=[]
    for filename in glob.glob(filedir+'/*.xml'):
        #print filename
        lines, ambiguous, ambiguouslevel2, nonambiguous, weird = readFileGetSyllableStressWords(filename)
        stats.append([lines, ambiguous, ambiguouslevel2, nonambiguous, weird])
        #print lines, ambiguous, ambiguouslevel2, nonambiguous, weird
    print stats

    import numpy as np
    print "Total number of lines:", np.sum([i[0] for i in stats])
    print "Mean of the number of lines:", np.mean([i[0] for i in stats])
    print "Total number of ambiguous lines:", np.sum([i[1] for i in stats])
    print "Mean of the number of ambiguous lines:", np.mean([i[1] for i in stats])
    print "Total number of ambiguous lines (level 2):", np.sum([i[2] for i in stats])
    print "Mean of the number of ambiguous lines (level 2):", np.mean([i[2] for i in stats])
    print "Total number of non-ambiguous lines:", np.sum([i[3] for i in stats])
    print "Mean of the number of non-ambiguous lines:", np.mean([i[3] for i in stats])
    print "Mean of the number of weird lines:", np.mean([i[4] for i in stats])
checkAmbiguity(sys.argv[1])

