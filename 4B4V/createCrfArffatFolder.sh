folder=$1

mkdir ${folder}
echo "" > ${folder}/newcorpus.crf

for i in /home/magirrezaba008/Dropbox/poetryCorpora/wholeCorpus/statsAndCorpora/poems_2015-11-30/*.xml;
do
  echo $i
  python newCorpusReader.py "$i" crf | tr "[:upper:]" "[:lower:]" >> ${folder}/newcorpus.crf
done

python crf2arff.py ${folder}/newcorpus.crf > ${folder}/newcorpus.arff

