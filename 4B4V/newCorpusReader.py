# -*- coding: utf-8 -*-

import sys
import os
from lxml import etree
import re
import itertools
import nltk
import HTMLParser
h = HTMLParser.HTMLParser()

import lexicon
HOME = os.environ['HOME']
lex=lexicon.lexicon(HOME+"/Dropbox/bertsobot/pronun/hiztegiak/newdic-ipa.txt")

fn={}
#fn['CLASS']=0
fn['NSL']=3

#fn['LS']=4
#fn['POS']=5

fn['WLEN']=6
fn['ISHEAVY']=13

#fn['LSBEF']=14
#fn['LSAFT']=15

fn['SNOW']=7
fn['SNOL']=16
fn['LASTCHAR']=8
fn['LAST2CHAR']=9
fn['LAST3CHAR']=10
fn['LAST4CHAR']=11
fn['LAST5CHAR']=12


'''
HELP FROM:
https://docs.python.org/2/library/xml.etree.elementtree.html#parsing-xml-with-namespaces
https://docs.python.org/2/library/xml.etree.elementtree.html
THE PROGRAM printFeaturesFromFile TAKES AS ARGUMENT AN XML FILE (FROM THE 4B4V POETRY CORPUS) AND A FORMAT (hunpos, crf, DLs2s, DLw2w).
IT PRINTS THE FILE IN THE SPECIFIED FORMAT.
  -hunpos: The format in which the hunpos-implementation works. It's an HMM-based tagger
  -crf: The format for the crfsuite program. CRF-based implementation.
  -DLs2s: This prints the poem in a special format to use in the char-rnn package (Syllable by syllable). Example: "that_= is_+ the_= ques_+ tion_="
  -DLw2w: This prints the poem in a special format to use in the char-rnn package (Word by word). Example: "that_= is_+ the_= question_+="
'''

'''
This file gets an XML file from the 4B4V repository with analyzed English poems and it extracts the syllables, words and stresses in tuples/words/lines/linegroups
'''
def readFileGetSyllableStressWords (filename, verbose=False):
    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    if "{" in tree.tag:
        namespace= re.sub("\}.*", "", tree.tag).replace("{","")
    else:
        namespace=''
    ns = {'ns':namespace}

    lineElements=[]
    lineGroupElements=[]
    linegroups = tree.findall("./ns:text/ns:body/", ns)
    for linegroup in linegroups:
        for line in linegroup.findall("./ns:l", ns):
            realscansions = line.attrib['real'].split("|")
            realscansions = [i.replace("(", "") for i in realscansions]
            realscansions = [i.replace(")", "") for i in realscansions]
            realscansions = [i.replace(" ", "") for i in realscansions]

            #REPLACE - with = to avoid ambiguity: Ah Sun-flower! weary of time,
            realscansions = [i.replace("-", "=") for i in realscansions]

            segV=[]
            for realscansion in realscansions:
                scannedline=""
                stressind=0
                segments = line.findall("./ns:seg", ns)
                for segno, seg in enumerate(segments): #EACH SEGMENT / EACH FOOT
                    for kno, k in enumerate(seg.itertext()): #EACH SEGMENT OR ELEMENT INSIDE SEGMENT
                        if hasChar(k):
                            scannedsegment,stressindloc=alignLineStress(realscansion[stressind:], k)
                            stressind=stressind+stressindloc
                            scannedline=scannedline+scannedsegment
                lineElements.append([extractInfo(i) for i in scannedline.split()])
                #Append a tuple with the form (stress, syllable, word) for each syllable
        lineGroupElements.append(lineElements)
        lineElements=[]
    return lineGroupElements

def filterword (string):
    return re.sub("[^A-Za-z\'\-\#]", "",string) #TODO (TOCHECK) Should we include #? Last chars.

#NEW VERSION
diphs=['ae', 'ai', 'au', 'ea', 'ee', 'ei', 'eo', 'eu', 'ia', 'ie', 'io', 'iu', 'oa', 'oe', 'oi', 'oo', 'ou', 'ua', 'ue', 'ui', 'uo']
#Diphthongs calculated using the program diphthong.py /home/magirrezaba008/Dropbox/thesis/unsupervisedScansion/hiawatha.txt
def isheavy(str):
    if str[-1] in 'bcdfghjklmnpqrstvxyz': #If the syllable ends in consonant, it is heavy
        return '1'
    elif re.sub("[^aeiou]","",str) in diphs: #If the nucleus (syllable without consonants) is a diphthong, it is heavy
        return '1'
    else: #Then, if neither of the previous conditions are satisfied, the syllable is light
        return '0'

def extractInfo(str):
    stresses=extractstresses(str)
    word=filterword(removestresses(str))
    syllables=re.split("[=|+]", str)[:-1]
    syllables=[filterword(i) for i in syllables]
    return zip(stresses,syllables,[word]*len(syllables))
    #For the input re=sem+ble=
    #this function returns [('=', 're', 'resemble'), ('+', 'sem', 'resemble'), ('=', 'ble', 'resemble')]

def alignLineStress (stresses, line):
  i=0
  pos=0
  lineind=0
  while i<len(line) and pos != -1:
    pos=line.find(" ",i)
    if pos != -1:
      line=line[:pos]+stresses[lineind]+line[pos:]
      lineind=lineind+1
      i=pos+2
    else:
      line=line+stresses[lineind]
      lineind=lineind+1

  return line, lineind


def extractstresses(s):
    return ''.join([i for i in s if i in '=+'])

def removestresses(s):
    return ''.join([i for i in s if i not in '=+'])

def hasChar (s):
    for c in s:
        if c.isalpha():
            return True
    return False

def lastchars(localw, localc):
    if (len(localw) < localc):
        return "#"*(localc-len(localw)) + localw
    else:
        return localw[-localc:]

def enrichFeatures (lgs):
    for indlg,lg in enumerate(lgs):
        for indl,l in enumerate(lg): #(Stress, syllable, word) tuple
            indsline=0
            wordlst=[w[0][2] for w in l]
            postags=nltk.pos_tag(wordlst)
            for indw,w in enumerate(l):
                for inds,s in enumerate(w):
                    word=s[2]
                    lgs[indlg][indl][indw][inds]+=(indsline,) #SN WITHIN LINE
                    lgs[indlg][indl][indw][inds]+=(lex.stressPatt(word),) #LEXICAL STRESS
                    lgs[indlg][indl][indw][inds]+=(postags[indw][1],) #POS-TAG OF WORD
                    lgs[indlg][indl][indw][inds]+=(len(word),) #WORD-LENGTH
                    lgs[indlg][indl][indw][inds]+=(inds,) # SYLLABLE INDEX INSIDE THE WORD
                    lgs[indlg][indl][indw][inds]+=(filterword(lastchars(word,1)),) #Last character
                    lgs[indlg][indl][indw][inds]+=(filterword(lastchars(word,2)),) #Last 2 characters
                    lgs[indlg][indl][indw][inds]+=(filterword(lastchars(word,3)),) #Last 3 characters
                    lgs[indlg][indl][indw][inds]+=(filterword(lastchars(word,4)),) #Last 4 characters
                    lgs[indlg][indl][indw][inds]+=(filterword(lastchars(word,5)),) #Last 5 characters
                    lgs[indlg][indl][indw][inds]+=(isheavy(word),) #Last 5 characters
                    indsline=indsline+1

    for indlg,lg in enumerate(lgs):
        for indl,l in enumerate(lg):
            for indw,w in enumerate(l):
                for inds,s in enumerate(w):
                    lsbef=''.join([k[0][4] for k in l[:indw]]) #JUST JOIN IT NOW (FW)
                    lsaft=''.join([k[0][4] for k in l[indw+1:]]) #JUST JOIN IT NOW (FW)
                    totnumsyllsline=l[-1][-1][3]+1
                    lgs[indlg][indl][indw][inds]+=(lsbef,)
                    lgs[indlg][indl][indw][inds]+=(lsaft,)
                    lgs[indlg][indl][indw][inds]+=(totnumsyllsline,)

    return lgs

def getStressFromWord (w):
  return ''.join([s[0] for s in w])
    
def getmappings (linegroups):
  pos={}
  ls={}
  for lineg in linegroups:
    for line in lineg:
      for word in line:
        for syl in word:
          ls[syl[4]]=ls.get(syl[4],0)+1
          pos[syl[5]]=pos.get(syl[5],0)+1
#  print pos, len(pos), sorted(pos, key=pos.get, reverse=True)[:25]
#  print ls, len(ls), sorted(ls, key=ls.get, reverse=True)[:25]
#  raw_input()  

  

  posdi=['ㅂ','ㅈ','ㄷ','ㄱ','ㅅ','ㅁ','ㄴ','ㅇ','ㄹ','ㅎ','ㅋ','ㅌ','ㅊ','ㅍ','ㅃ','ㅉ','ㄸ','ㄲ','ㅆ','ㅛ','ㅕ','ㅑ','ㅐ','ㅔ']
  posdi = [i.decode("utf8") for i in posdi]
  lsdi=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y']

  #http://stackoverflow.com/questions/209840/map-two-lists-into-a-dictionary-in-python
  return dict(zip(sorted(pos, key=pos.get, reverse=True)[:25],posdi)).get, dict(zip(sorted(ls, key=ls.get, reverse=True)[:25],lsdi)).get

from compiler.ast import flatten
def printFeatures (lgs, format):
    UNKCHARLS="Z"
    UNKCHARPOS="ㅗ".decode("utf8")
    if format == 'DLs2s':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                print ' '.join([s[1]+"_"+s[0] for w in l for s in w]).encode("utf8")
            print
        print
    elif format == 'DLs2srtl':
        mappos, mapls = getmappings(lgs)
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(reversed(lg)):
                #print ' '.join([s[0]+"_"+s[1].lower() for w in reversed(l) for s in reversed(w)]).encode("utf8")
                print ' '.join([s[1].lower()+"_"+s[0] for w in reversed(l) for s in reversed(w)]).encode("utf8")
            print
        print
    elif format == 'DL2featrtl':
        mappos, mapls = getmappings(lgs)
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(reversed(lg)):
                #print ' '.join([s[0]+"_"+s[1].lower() for w in reversed(l) for s in reversed(w)]).encode("utf8")
                print ' '.join([s[1].lower()+mappos(s[5],UNKCHARPOS)+mapls(s[4],UNKCHARLS)+"_"+s[0] for w in reversed(l) for s in reversed(w)]).encode("utf8")
            print
        print
    elif format == 'DL2feat':
        mappos, mapls = getmappings(lgs)
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                print ' '.join([s[1].lower()+mappos(s[5],UNKCHARPOS)+mapls(s[4],UNKCHARLS)+"_"+s[0] for w in l for s in w]).encode("utf8")
            print
        print
    elif format == 'DLw2w':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                print ' '.join([w[0][2]+"_"+getStressFromWord(w) for w in l]).encode("utf8")
            print
        print
    elif format == 'raw':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                print ' '.join([w[0][2] for w in l]).encode("utf8")
            print
        print
    elif format == 'zeuscansion':
        scansions=[]
        for indlg,lg in enumerate(lgs):
            poetrylinebef=""
            for indl,l in enumerate(lg):
                poetryline=' '.join([w[0][2] for w in l]).encode("utf8")
                if poetrylinebef != poetryline:
                  if scansions != []:
                    print '|'.join(scansions)
                    scansions=[]
                  print poetryline+"\t",
                scansions.append(''.join([syll[0] for w in l for syll in w]).replace("=","-").encode("utf8"))
                poetrylinebef=poetryline
            if indl == len(lg)-1:
              print '|'.join(scansions)
              scansions=[]
            print
        print
    elif format == 'crf':
        word=True
        syllable=True
        syllablespace=False
        pos=True
        lexicalstress=True
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                for indw,w in enumerate(l):
                    for inds,s in enumerate(w): #(Stress0, syllable1, word2, syllnoinline3, LS4, POS-TAG5, word_length6, syllnowithinword7, lastchar8, last2chars9, last3chars10, last4chars11, last5chars12, ISHEAVY[1|0]13, LSBEFORE14, LSAFTER15, numberofsyllsinline16)
                        if word:
                            wordwindow= flatten(list(windowwords(l,indw,2, 5))) #WORD WINDOW +-5
#                            wordwindow= flatten(list(windowwordsminmax(l,indw,2, -2, 4))) #WORD WINDOW +-5
                            wordwindow= ["w["+str(wno)+"]="+word.lower() for wno,word in enumerate(wordwindow)]
                        else:
                            wordwindow=[]

                        if lexicalstress:
                            lswindow= flatten(list(windowwords(l,indw,4, 5))) #LEXICAL STRESS WINDOW +-5
                            lswindow= ["LS["+str(lsno)+"]="+ls for lsno,ls in enumerate(lswindow)]
                        else:
                            lswindow=[]

                        if pos:
                            poswindow= flatten(list(windowwords(l,indw,5, 5))) #POS-TAG WINDOW +-5
                            poswindow= ["postag["+str(sno)+"]="+syl for sno,syl in enumerate(poswindow)]
                        else:
                            poswindow=[]

                        if syllablespace:
                            syllwspacewindow=flatten(list(windowsyllswSpaces(l,indw, inds,1, 10))) #SYLLABLE WINDOW +-10 (including spaces)
                            syllwspacewindow= ["s["+str(sno)+"]="+syl.lower() for sno,syl in enumerate(syllwspacewindow)]
                        else:
                            syllwspacewindow=[]

                        if syllable:
                            #syllwindow= flatten(list(windowsylls(l,indw, inds,1, 10))) #SYLLABLE WINDOW +-10
                            syllwindow= flatten(list(windowsyllsminmax(l,indw, inds,1, -10,10))) #SYLLABLE WINDOW -1...3
                            syllwindow= ["s["+str(sno)+"]="+syl.lower() for sno,syl in enumerate(syllwindow)]
                        else:
                            syllwindow=[]


                        #print (s[0]+"\t"+'\t'.join(wordwindow+lswindow+poswindow+syllwindow+syllwspacewindow+[unicode(s[fn[feat]]) for feat in fn])).encode("utf8")
                        print (s[0]+"\t"+'\t'.join(wordwindow+lswindow+poswindow+syllwindow+syllwspacewindow+[unicode(feat+"="+str(s[fn[feat]])).lower() for feat in fn])).encode("utf8")

                print
            print
        print
    elif format == 'glampetagger':
        syllablespace=False

        def getPreviousStress (sin, win, sen):
          if sin==0 and win == 0:
            return "#"
          elif sin == 0 and win != 0:
            return sen[win-1][-1][0]
          elif sin != 0:
            return sen[win][sin-1][0]

        def glampestress(syllind, wind, seq):
          prevst = getPreviousStress(syllind, wind, seq)
          currst = seq[wind][syllind][0]
#          print "STRESS:", seq[wind][syllind][0]
#          print "syllind:",syllind
#          print "wind:",wind
#          print "previousst:",prevst
#          raw_input()

          if currst == '+' and prevst == '+':
            return 'I-ST'
          elif currst == '=' and prevst == '=':
            return 'I-UN'
          elif currst == '=':
            return 'B-UN'
          elif currst == '+':
            return 'B-ST'
          else:
            return 'O'


        word=False
        syllable=True
        pos=True
        lexicalstress=True
        wordwinsize=0#5
        syllwinsize=0#10
        lexswinsize=0#5
        postwinsize=0#5
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                for indw,w in enumerate(l):
                    for inds,s in enumerate(w): #(Stress0, syllable1, word2, syllnoinline3, LS4, POS-TAG5, word_length6, syllnowithinword7, lastchar8, last2chars9, last3chars10, last4chars11, last5chars12, ISHEAVY[1|0]13, LSBEFORE14, LSAFTER15, numberofsyllsinline16)
                        if word:
                            wordwindow= flatten(list(windowwords(l,indw,2, wordwinsize))) #WORD WINDOW +-5
#                            wordwindow= flatten(list(windowwordsminmax(l,indw,2, -2, 4))) #WORD WINDOW +-5
                            wordwindow= ["w["+str(wno)+"]="+word.lower() for wno,word in enumerate(wordwindow)]
                        else:
                            wordwindow=[]

                        if lexicalstress:
                            lswindow= flatten(list(windowwords(l,indw,4, lexswinsize))) #LEXICAL STRESS WINDOW +-5
                            lswindow= ["LS["+str(lsno)+"]="+ls for lsno,ls in enumerate(lswindow)]
                        else:
                            lswindow=[]

                        if pos:
                            poswindow= flatten(list(windowwords(l,indw,5, postwinsize))) #POS-TAG WINDOW +-5
                            poswindow= ["postag["+str(sno)+"]="+syl for sno,syl in enumerate(poswindow)]
                        else:
                            poswindow=[]

                        if syllablespace:
                            syllwspacewindow=flatten(list(windowsyllswSpaces(l,indw, inds,1, syllwinsize))) #SYLLABLE WINDOW +-10 (including spaces)
                            syllwspacewindow= ["s["+str(sno)+"]="+syl.lower() for sno,syl in enumerate(syllwspacewindow)]
                        else:
                            syllwspacewindow=[]

                        if syllable:
                            #syllwindow= flatten(list(windowsylls(l,indw, inds,1, 10))) #SYLLABLE WINDOW +-10
                            syllwindow= flatten(list(windowsyllsminmax(l,indw, inds,1, -syllwinsize,syllwinsize))) #SYLLABLE WINDOW -1...3
                            syllwindow= ["s["+str(sno)+"]="+syl.lower() for sno,syl in enumerate(syllwindow)]
                        else:
                            syllwindow=[]


                        #print (s[0]+"\t"+'\t'.join(wordwindow+lswindow+poswindow+syllwindow+syllwspacewindow+[unicode(s[fn[feat]]) for feat in fn])).encode("utf8")
                        print ('\t'.join(wordwindow+lswindow+poswindow+syllwindow+syllwspacewindow+[unicode(feat+"="+str(s[fn[feat]])).lower() for feat in fn])+"\t"+glampestress(inds, indw, l)).encode("utf8")

                print
            print
        print
    elif format == 'crfword':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                for indw,w in enumerate(l):
                    print (getStressFromWord(w)+"\t"+w[0][2].lower()+"\t"+w[0][4]+"\t"+w[0][5]).encode("utf8")
                print
            print
        print
    elif format == 'tensorflow-word':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                print ' '.join([w[0][2].lower() for w in l]),
                print "\t",
                print ' '.join([getStressFromWord(w) for w in l])
            print
        print
    elif format == 'tensorflow-syllable-syllable':
        scansions=[]
        for indlg,lg in enumerate(lgs):
            poetrylinebef=""
            for indl,l in enumerate(lg):
                poetryline=' '.join([syll[1].lower() for w in l for syll in w]).encode("utf8")
                if poetrylinebef != poetryline:
                  if scansions != []:
                    print '|'.join(scansions)
                    scansions=[]
                  print poetryline+"\t",
                scansions.append(' '.join([syll[0] for w in l for syll in w]).encode("utf8"))
                poetrylinebef=poetryline
            if indl == len(lg)-1:
              print '|'.join(scansions)
              scansions=[]
            print
        print
    elif format == 'hunpos':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                for indw,w in enumerate(l):
                    for inds,s in enumerate(w):
                        print (' '.join([unicode(s[fn[feat]]) for feat in fn])+"\t"+s[0]).encode("utf8") #Nonsense
                    
                print
            print
        print
    elif format == 'hunposword':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                for indw,w in enumerate(l):
                    print (w[0][2]+"\t"+getStressFromWord(w)).encode("utf8")
                print
            print
        print
    elif format == 'syllablegs':
        for indlg,lg in enumerate(lgs):
            for indl,l in enumerate(lg):
                for indw,w in enumerate(l):
                    print (re.sub("[^A-Za-z]","", w[0][2]).lower()+"\t"+re.sub("[^A-Za-z\.]","", ".".join([s[1] for s in w]).lower())).encode("utf8")


        #print "1\t+" #DIGIT NECESARY FOR HUNPOS-TRAIN
        #print "1\t-" #DIGIT NECESARY FOR HUNPOS-TRAIN

'''
This program will get as input an element *el* with syllables in it, a timestep *ts*, an element number *elno*, which will refer to the element inside the tuple that represents each syllable and a window size, *windowsize*
It will return the *windowsize*x2+1 elements surrounding the el[ts] element at index elno
'''
def windowwords (el, ts, elno, windowsize):
    ENDBOUND="#"
    elts=el[ts]
    wordsts=elts[0][elno]
    eltsplus=el[ts+1:(ts+1+windowsize)]+[ENDBOUND]*(windowsize-len(el[ts+1:(ts+1+windowsize)]))
    wordstsplus = [i[0][elno] if i!= "#" else "#" for i in eltsplus]
    
    leftbound=ts-windowsize
    rightbound=ts
    if leftbound<0:
        eltsminus=[ENDBOUND]*abs(leftbound)+el[:rightbound]
    else:
        eltsminus=el[leftbound:rightbound]
    wordstsminus=[i[0][elno] if i!= "#" else "#" for i in eltsminus]

    return wordsts, wordstsplus, wordstsminus


def windowwordsminmax (el, ts, elno, minind, maxind):
    ENDBOUND="#"
    elts=el[ts]
    wordsts=elts[0][elno]
    eltsplus=el[ts+1:(ts+1+maxind)]+[ENDBOUND]*(maxind-len(el[ts+1:(ts+1+maxind)]))
    wordstsplus = [i[0][elno] if i!= "#" else "#" for i in eltsplus]
    
    leftbound=ts+minind
    rightbound=ts
    if leftbound<0:
        eltsminus=[ENDBOUND]*abs(leftbound)+el[:rightbound]
    else:
        eltsminus=el[leftbound:rightbound]
    wordstsminus=[i[0][elno] if i!= "#" else "#" for i in eltsminus]

    return wordsts, wordstsplus, wordstsminus

    
def windowsylls(el,wts, sts,elno, windowsize):
    ENDBOUND="#"
    localwts=wts
    localsts=sts
    syllts=el[localwts][localsts][elno]

    filled=0
    leftsylls=[]
    while (localwts+localsts>0 and filled<windowsize):
        localsts=localsts-1
        if localsts<0:
            localwts=localwts-1
            localsts=len(el[localwts])-1
        leftsylls.insert(0, el[localwts][localsts])
        filled=filled+1
    syllstsminus = [i[elno] for i in leftsylls]
    syllstsminus=[ENDBOUND]*(abs(len(syllstsminus)-windowsize))+syllstsminus

    filled=0
    rightsylls=[]
    localwts=wts
    localsts=sts
#    print localwts, localsts
    while (localwts<len(el)-1 and filled<windowsize):
        localsts=localsts+1
        if localsts >=len(el[localwts]):
            localwts=localwts+1
            localsts=0
        rightsylls.append(el[localwts][localsts])
#        print "Append this:", localwts, localsts, el[localwts][localsts], len(el)
        filled=filled+1
    syllstsplus = [i[elno] for i in rightsylls]
    syllstsplus=syllstsplus+[ENDBOUND]*(abs(len(syllstsplus)-windowsize))

    return syllts, syllstsminus, syllstsplus

#flatten(list(windowsylls(l,indw, inds,1, 10))) #SYLLABLE WINDOW +-10
def windowsyllsminmax(el,wts, sts,elno, minind, maxind):
    windowsizeleft=(-minind)
    windowsizeright=maxind
    ENDBOUND="#"
    localwts=wts
    localsts=sts
    syllts=el[localwts][localsts][elno]

    filled=0
    leftsylls=[]
    while (localwts+localsts>0 and filled<windowsizeleft):
        localsts=localsts-1
        if localsts<0:
            localwts=localwts-1
            localsts=len(el[localwts])-1
        leftsylls.insert(0, el[localwts][localsts])
        filled=filled+1
    syllstsminus = [i[elno] for i in leftsylls]
    syllstsminus=[ENDBOUND]*(abs(len(syllstsminus)-windowsizeleft))+syllstsminus

    filled=0
    rightsylls=[]
    localwts=wts
    localsts=sts
#    print localwts, localsts
    while (localwts<len(el)-1 and filled<windowsizeright):
        localsts=localsts+1
        if localsts >=len(el[localwts]):
            localwts=localwts+1
            localsts=0
        rightsylls.append(el[localwts][localsts])
#        print "Append this:", localwts, localsts, el[localwts][localsts], len(el)
        filled=filled+1
    syllstsplus = [i[elno] for i in rightsylls]
    syllstsplus=syllstsplus+[ENDBOUND]*(abs(len(syllstsplus)-windowsizeright))

    return syllts, syllstsminus, syllstsplus


def windowsyllswSpaces(el,wts, sts,elno, windowsize):
    ENDBOUND="#"
    SPACE="#SP#"
    localwts=wts
    localsts=sts
    syllts=el[localwts][localsts][elno]

#    print "LEFT!"
    filled=0
    leftsylls=[]
    while (localwts+localsts>0 and filled<windowsize):
        localsts=localsts-1
        if localsts<0:
            localwts=localwts-1
            localsts=len(el[localwts])-1
        #print el[localwts][localsts], localwts, localsts, len(el[localwts]), localsts==len(el[localwts])-1
        if localsts==len(el[localwts])-1:
#            print "ADD SPBOUND!"
            filled=filled+1
            leftsylls.insert(0, SPACE)
        if filled<windowsize:
            leftsylls.insert(0, el[localwts][localsts])
            filled=filled+1
#    print "\t1",leftsylls
    syllstsminus = [i[elno] if i != SPACE else i for i in leftsylls]
#    print "\t2",syllstsminus
    syllstsminus=[ENDBOUND]*(abs(len(syllstsminus)-windowsize))+syllstsminus

#    print "RIGHT!"
    filled=0
    rightsylls=[]
    localwts=wts
    localsts=sts
#    print localwts, localsts
    while (localwts<len(el)-1 and filled<windowsize):
        if localsts==len(el[localwts])-1:
#            print "ADD SPBOUND!"
            filled=filled+1
            rightsylls.append(SPACE)
        localsts=localsts+1
        if localsts >=len(el[localwts]):
            localwts=localwts+1
            localsts=0
#        print el[localwts][localsts], localwts, localsts, len(el[localwts]), localsts==len(el[localwts])-1, [i[elno] for i in rightsylls]
        if filled<windowsize:
            rightsylls.append(el[localwts][localsts])
#        print "Append this:", localwts, localsts, el[localwts][localsts], len(el)
            filled=filled+1
    syllstsplus = [i[elno] if i != SPACE else i for i in rightsylls]
    syllstsplus=syllstsplus+[ENDBOUND]*(abs(len(syllstsplus)-windowsize))

    return syllts, syllstsminus, syllstsplus

def printFeaturesFromFile (file, format):
    res = readFileGetSyllableStressWords(file)
    res = enrichFeatures(res)
    printFeatures(res, format)


if __name__ == "__main__":
  printFeaturesFromFile(sys.argv[1], sys.argv[2])

