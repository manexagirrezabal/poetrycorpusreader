
import sys
import os
import glob
from lxml import etree
import re
import itertools
import operator
import nltk
import HTMLParser
import newCorpusReader
h = HTMLParser.HTMLParser()
syllableStressPriors={}

def getInfoFile (filename, verbose=False):


    lineGroupMeter=False #python corpusInfo.py ~/Dropbox/poetryCorpora/wholeCorpus/statsAndCorpora/poems_2015-11-30/ | sort | uniq -c



    parser = etree.XMLParser(encoding='utf-8')
    tree = etree.parse(filename, parser=parser, base_url=None).getroot()

    if "{" in tree.tag:
        namespace= re.sub("\}.*", "", tree.tag).replace("{","")
    else:
        namespace=''
    ns = {'ns':namespace}

    lineElements=[]
    lineGroupElements=[]
    linegroups = tree.findall("./ns:text/ns:body/", ns)

    global syllableStressPriors

    for linegroup in linegroups:
        
        if 'met' in linegroup.attrib:
          meter=re.sub("\(.*", "", linegroup.attrib['met'])
        else:
          meter=re.sub("\(.*", "", linegroup.attrib['real'])

        if lineGroupMeter:
          print meter

        for line in linegroup.findall("./ns:l", ns):
            realscansions = line.attrib['real'].split("|")
            realscansions = [i.replace("(", "") for i in realscansions]
            realscansions = [i.replace(")", "") for i in realscansions]
            realscansions = [i.replace(" ", "") for i in realscansions]

            #REPLACE - with = to avoid ambiguity: Ah Sun-flower! weary of time,
            realscansions = [i.replace("-", "=") for i in realscansions]

            #print meter, realscansions, "=",''.join(realscansions).count("="),"+",''.join(realscansions).count("+")
            unstrstrtuple=(''.join(realscansions).count("="),''.join(realscansions).count("+"))
            syllableStressPriors[meter]=tuple(map(operator.add, syllableStressPriors.get(meter,(0,0)), unstrstrtuple))

            segV=[]
            for realscansion in realscansions:
                scannedline=""
                stressind=0
                segments = line.findall("./ns:seg", ns)
                for segno, seg in enumerate(segments): #EACH SEGMENT / EACH FOOT
                    for kno, k in enumerate(seg.itertext()): #EACH SEGMENT OR ELEMENT INSIDE SEGMENT
                        if newCorpusReader.hasChar(k):
                            scannedsegment,stressindloc=newCorpusReader.alignLineStress(realscansion[stressind:], k)
                            stressind=stressind+stressindloc
                            scannedline=scannedline+scannedsegment
                lineElements.append([newCorpusReader.extractInfo(i) for i in scannedline.split()])
                #Append a tuple with the form (stress, syllable, word) for each syllable
        lineGroupElements.append(lineElements)
        lineElements=[]
    print syllableStressPriors
    return lineGroupElements

def count (str):
  return (str.count("="),str.count("+"))

def printInfo(direct):
  for filename in glob.glob(direct+'*.xml'):
    #print filename
    getInfoFile(filename)
  for el in syllableStressPriors:
    print el, syllableStressPriors[el],syllableStressPriors[el][0],syllableStressPriors[el][1],
    print float(syllableStressPriors[el][0])/sum(syllableStressPriors[el]), float(syllableStressPriors[el][1])/sum(syllableStressPriors[el])

if __name__ == "__main__":
  printInfo(sys.argv[1])
  
