
#This script takes the Spanish poetry dataset with 64 features and transforms it
#for the Word to stresspattern task.
#
#The input is the dataset in CRFsuite format and the output must be the following:
#
#word <TAB> stressPattern <TAB> snow...


import sys

f=open(sys.argv[1])
lines=[line.decode("utf8").rstrip().split("\t") for line in f]
f.close()

toprint=[""]*3
toprinttext=""
for line in lines:
#  if line!=['']:
#    toprint=line[0]+"\t"+line[1]+"\t"+line[11]
  if line == [''] and toprint==['', '', '']:
    toprint=[""]*3
    print ""
  elif line == [''] and toprint!=['', '', '']:
    toprinttext='\t'.join(toprint)
    print toprinttext.encode("utf8")
    print
    toprint=[""]*3
  elif line != [''] and toprint==['', '', '']:
    toprint[0]=line[0]
    toprint[1]=line[1]
    toprint[2]=line[11].replace("w[0]=","")
  elif line != [''] and toprint!=['', '', '']:
    if line[1]=='snow=0':
      toprinttext='\t'.join(toprint)
      print toprinttext.encode("utf8")
      toprint[0]=line[0]
      toprint[1]=line[1]
      toprint[2]=line[11].replace("w[0]=","")
    else:
      toprint[0]=toprint[0]+line[0]

