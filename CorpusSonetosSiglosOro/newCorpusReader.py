
import sys
from lxml import etree
import re
import string
import postagger
from ixapipes import just_postag
from spanishstress import setstress

#Rule-based syllabifier written in foma
import foma
HOMEDIR="/home/magirrezaba008/"
FST=foma.FST()
FSM=FST.load(HOMEDIR+"/Dropbox/thesis/syllableGoldStandard/ruleSyllabifiers/silabaEs.fst")
def analyzeWord (w):
  res=FSM.apply_down(w)
  if res is None:
    return '+?'
  else:
    return res

#Flatten list, getting a list of lists, return a simple list of depth 1
def flatten(x):
  result = []
  for el in x:
    result=result+el
  return result


#Flatten list, getting a list of lists, return a simple list of depth 1
#+WordBoundary
def flattenWordB(x):
  result = []
  for el in x:
    result=result+el
    result[-1]=result[-1]+"."
  return result

#isheavy function, simple heuristic to calculate syllables stress in English.
diphs=[u'aa', u'ae', u'ai', u'ao', u'au', u'ea', u'ee', u'ei', u'eo', u'eu', u'ia', u'ie', u'ii', u'io', u'iu', u'oa', u'oe', u'oi', u'oo', u'ou', u'ua', u'ue', u'ui', u'uo']
#Diphthongs calculated using the program diphthong.py ~/CorpusSonetosSigloDeOro/spanishpoetry.txt %(Whole spanish poetry corpus)
def isheavy(str):
    if str[-1] in 'bcdfghjklmnpqrstvxyz': #If the syllable ends in consonant, it is heavy
        return '1'
    elif re.sub("[^aeiou]","",str) in diphs: #If the nucleus (syllable without consonants) is a diphthong, it is heavy
        return '1'
    else: #Then, if neither of the previous conditions are satisfied, the syllable is light
        return '0'

#This function returns the last N characters of the word W
#lastchars(W,N)
def lastchars(localw, localc):
    if (len(localw) < localc):
        return "#"*(localc-len(localw)) + localw
    else:
        return localw[-localc:]

#This function returns the next n elements
def nextWords (ls, current, n):
  result=[]
  if current+1+n>=len(ls):
    for i in xrange(current+1,len(ls)):
      #print "Next:",ls[i]
      result.append(ls[i])
#    print (current+1+n)-len(ls) #Number of empty elements that we should add
    for i in xrange(0,(current+1+n)-len(ls)):
      #print "Next: #"
      result.append("#")
  else:
    for i in xrange(current+1,current+1+n):
      #print "Next:",ls[i]
      result.append(ls[i])
  return result

#This function returns the previous n elements
def previousWords(ls,current,n):
  result=[]
  if current-n<0:
    for i in xrange(0,current):
#      print "Previous:",ls[i],i, current-n
      result.append(ls[i])
    for i in xrange(0,abs(current-n)):
#      print "Previous: #"
      result.append("#")
  else:
    for i in xrange(current-n, current):
#      print "Previous:", ls[i], i
      result.append(ls[i])
  return result

def readFile(filename):
  parser = etree.XMLParser()
  tree = etree.parse(filename, parser=parser, base_url=None).getroot()

  if "{" in tree.tag:
    namespace= re.sub("\}.*", "", tree.tag).replace("{","")
  else:
    namespace=''
  ns = {'ns':namespace}

#  lineElements=[]
#  lineGroupElements=[]
  linegroups = tree.findall("./ns:text/ns:body/ns:lg", ns)
  for linegroup in linegroups:
    for line in linegroup:
      cleanline = ''.join([char for char in line.text if char not in string.punctuation]) #Remove punctuation
      cleanline=' '.join(cleanline.split())
      syllabifiedVerse=analyzeWord(cleanline.encode("utf8")).next().decode("utf8") #Syllabify line using a rule-based syllabifier for Spanish
      syllabifiedVerseList=[word.split(".") for word in syllabifiedVerse.split(" ")]
      syllableList=flatten(syllabifiedVerseList)
      postag_list=just_postag(cleanline)

      realscansions=line.attrib['real'].split("|")
      for realscansion in realscansions:
        #We are going to assert that the number of syllables in the 'real' attribute and the number of syllables in the line will be the same
#        print syllableList,realscansions
        assert len(syllableList)==len(realscansion)

        indsyll_line=0
        for indword, word in enumerate(syllabifiedVerseList):
          joinedword=''.join(word)
          for indsyll, syll in enumerate(word):
            toprint=[]
#            toprint.append(realscansion[indsyll_line])
#            toprint.append("snow="+str(indsyll))
#            toprint.append("nsl="+str(indsyll_line))
#            toprint.append("snol="+str(len(syllableList)))
#            toprint.append("isheavy="+str(isheavy(syll)))
#            toprint.append("lastchar="+lastchars(joinedword,1).lower())
#            toprint.append("last2char="+lastchars(joinedword,2).lower())
#            toprint.append("last3char="+lastchars(joinedword,3).lower())
#            toprint.append("last4char="+lastchars(joinedword,4).lower())
#            toprint.append("last5char="+lastchars(joinedword,5).lower())
#            toprint.append("wlen="+str(len(joinedword)))

#            toprint.append("w[0]="+joinedword.lower())
#            toprint.append("\t".join(["w["+str(-indelement-1)+"]="+''.join(element) for indelement, element in enumerate(previousWords(syllabifiedVerseList, indword, 5))]).lower())
#            toprint.append("\t".join(["w["+str( indelement+1)+"]="+''.join(element) for indelement, element in enumerate(    nextWords(syllabifiedVerseList, indword, 5))]).lower())

#            toprint.append("s[0]="+syllabifiedVerseList[indword][indsyll])
#            toprint.append("\t".join(["s["+str(-indelement-1)+"]="+''.join(element) for indelement, element in enumerate(previousWords(syllableList, indsyll_line, 10))]).lower())
#            toprint.append("\t".join(["s["+str( indelement+1)+"]="+''.join(element) for indelement, element in enumerate(nextWords(syllableList, indsyll_line, 10))]).lower())

#            toprint.append("postag[0]="+postag_list[indword])
#            toprint.append("\t".join(["postag["+str(-indelement-1)+"]="+''.join(element) for indelement, element in enumerate(previousWords(postag_list, indword, 5))]))
#            toprint.append("\t".join(["postag["+str( indelement+1)+"]="+''.join(element) for indelement, element in enumerate(nextWords(postag_list, indword, 5))]))

#            previousstresses=[setstress(element) if element!='#' else ['#'] for element in previousWords(syllabifiedVerseList, indword, 5)]
#            nextstresses=[setstress(element) if element!='#' else ['#'] for element in nextWords(syllabifiedVerseList, indword, 5)]
#            toprint.append("LS[0]="+''.join(setstress(word)))
#            toprint.append("\t".join(["LS["+str(-indelement-1)+"]="+''.join(element) for indelement, element in enumerate(previousstresses)]))
#            toprint.append("\t".join(["LS["+str( indelement+1)+"]="+''.join(element) for indelement, element in enumerate(nextstresses)]))

            if indsyll+1==len(word):
              WB="#"
            else:
              WB=""
            toprint.append(syllabifiedVerseList[indword][indsyll]+WB)
            toprint.append(realscansion[indsyll_line])

            print "\t".join(toprint).encode("utf8")
#            print
            indsyll_line=indsyll_line+1
        print
    print
  print
  return [],[]

def main():
  readFile(sys.argv[1])




if __name__== '__main__':
  main()

