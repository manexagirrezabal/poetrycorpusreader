from nltk.tag import HunposTagger
#http://www.nltk.org/_modules/nltk/tag/hunpos.html

#The model is trained on http://www.cs.upc.edu/~nlp/wikicorpus/
ht=HunposTagger("/home/magirrezaba008/WikiCorpusSpanish/model.wikicorp.model")

def tag (sent):
  return ht.tag(sent)

if __name__== '__main__':
  print tag(['El','chico','se','fue','al','circo','.'])


