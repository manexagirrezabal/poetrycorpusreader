#!/usr/bin/env python
# -*- coding:  utf8

import socket


#This sends a request over TCP to the ports 5000 and 5001 so that ixa-pipes tokenizes and Part-of-speech tags text
#We need, then, to have two ixa-pipes servers:
#Tokenizer: java -jar ixa-pipe-tok/target/ixa-pipe-tok-1.8.4.jar server -p 5000 -l es
#Part-of-speech tagger: java -jar ixa-pipe-pos/target/ixa-pipe-pos-1.5.1-exec.jar server -l es -p 5001 -m ~/train/morph-models-1.5.0/es/es-pos-perceptron-autodict01-ancora-2.0.bin -lm ~/train/morph-models-1.5.0/es/es-lemma-perceptron-ancora-2.0.bin -o conll

#Info about ixa-pipes: http://ixa2.si.ehu.es/ixa-pipes/
#Info about ixa-pipes client: https://github.com/ixa-ehu/ixa-pipe-pos/blob/master/src/main/java/eus/ixa/ixa/pipe/pos/CLI.java
#Info about TCP communication: https://wiki.python.org/moin/TcpCommunication


TCP_IP = '127.0.0.1'
TCP_TOK_PORT = 5000 #This is the port in which the tokenizer is working
TCP_POS_PORT = 5001 #This is the port in which the POS-tagger is working
BUFFER_SIZE = 1024

def tokenize (text):
  if text[-1]!="\n":
    text=text+"\n"
  text=text+"<ENDOFDOCUMENT>\n"

  s_tok = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s_tok.connect((TCP_IP, TCP_TOK_PORT))
  s_tok.send(text.encode("utf8"))
  result_tok=""
  data = s_tok.recv(BUFFER_SIZE)
  while data!='':
    result_tok=result_tok+data
    data = s_tok.recv(BUFFER_SIZE)
  s_tok.close()
  return result_tok.decode("utf8")

def postag (naf_input):
  s_pos = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s_pos.connect((TCP_IP, TCP_POS_PORT))
  s_pos.send(naf_input.encode("utf8"))
  naf_output=""
  data = s_pos.recv(BUFFER_SIZE)
  while data!='':
    naf_output=naf_output+data
    data = s_pos.recv(BUFFER_SIZE)
  s_pos.close()
  return naf_output.decode("utf8")

def whole_postag(text):
  return postag(tokenize (text))

def just_postag (text):
  result=whole_postag(text)
  return [line.split("\t")[2] for line in result.split("\n") if line]

if __name__== '__main__':
  print (just_postag("Hola, pequeño mundo!"))

