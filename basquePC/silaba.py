import foma
import sys
import os
import re

HOME = os.environ['HOME']

FST=foma.FST()
FSM=FST.load(HOME+"/Dropbox/thesis/syllableGoldStandard/ruleSyllabifiers/silabaEus.fst")
def analyzeWord (w):
  res=FSM.apply_down(w)
  if res is None:
    return '+?'
  else:
    return next(res)

def word2syllables(w):
  return analyzeWord(w).split(".")

def main():
    print analyzeWord("ta lehenengo urtian"),word2syllables("ta lehenengo urtian")
    print analyzeWord("geu etxe ate"),word2syllables("geu etxe ate")
    print analyzeWord("langilearen semea izan ahal naiz ni"),word2syllables("langilearen semea izan ahal naiz ni")


if __name__ == '__main__':main()

